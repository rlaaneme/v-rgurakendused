<?php 
	// http://enos.itcollege.ee/~ttanav/VRI/2013/Praktikumid/prax08/ylesanne.html 
	function autendi() {
		global $myurl;
		$errors=array();
		$username="";
		$passwd="";
		if (isset($_POST['username']) && $_POST['username']!="") {
		  $username=$_POST['username'];
		} else {
		  $errors[]="Kasutajanimi puudu";
		}
		if (isset($_POST['passwd']) && $_POST['passwd']!="") {
		  $passwd=$_POST['passwd'];
		} else {
		  $errors[]="parool puudu";	
		}	
		if (empty($errors)){
		  // vigu polnud, kontrolli infot
		  if ($username=="kasutaja" && $passwd=="parool"){
			// õige info, tulevikus saab see olema turvalisem
			//kas kasutaja on sisselogitud 
			$_SESSION['username']=$username;	
			header("Location: $myurl");
			$_SESSION['notices'][]="Olete edukalt sisse logitud!";
		  } else {
			// vale info, kuva uuesti login leht
			$errors[]="Vale info, proovi uuesti.";	
			include("htmlsisu/login.php");
		  }	
		} else {
		  // vead, kuva logisisse vorm veateatega	
		  include("htmlsisu/login.php");
		}
	}

		
	// http://enos.itcollege.ee/~ttanav/VRI/2013/Praktikumid/prax09/ylesanne.html
	function lopeta_sessioon(){
		$_SESSION = array();
		if (isset($_COOKIE[session_name()])) {
		 setcookie(session_name(), '', time()-42000, '/');
		}
		session_destroy();
	}

	function logout() {
		global $myurl;
		lopeta_sessioon();
		header("Location: $myurl");
	}
?>